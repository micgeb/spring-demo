package com.gebarowski.legacy.coach;

public interface Coach {
    public String getDailyWorkout();

    public String getDailyFortune();
}
